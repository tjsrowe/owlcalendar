FROM node:12.10.0-alpine


WORKDIR /owlcalendar

COPY package.json /owlcalendar/package.json
COPY package-lock.json /owlcalendar/package-lock.json

RUN npm install
RUN npm install md5

RUN mkdir /owlcalendar/data
RUN mkdir /owlcalendar/data/owl
RUN mkdir -p /owlcalendar/data/contenders/2018
RUN mkdir -p /owlcalendar/data/contenders/2019
RUN mkdir -p /owlcalendar/data/trials/2018
RUN mkdir -p /owlcalendar/data/trials/2019
RUN mkdir -p /owlcalendar/data/worldcup/2017
RUN mkdir -p /owlcalendar/data/worldcup/2018

#COPY data/* /owlcalendar/data/

COPY *.js /owlcalendar/
COPY *.json /owlcalendar/

EXPOSE 3001

CMD ["node", "app.js"]
