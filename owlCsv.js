const owl = require('./owlcalendar');

var processCalendar = async function() {
	var config = owl.config;
	owl.checkDataCachedForConfig(config.calendars[0], onCsvBuild);
	
};

var onCsvBuild = function(data) {
	if (data && data.data.stages) {
		var stages = data.data.stages;
		var teamSchedule = {};
		var teamsList = [];
		for (var i = 0;i < stages.length;i++) {
			var stage = stages[i];
			addStageToSchedule(stage, teamSchedule, teamsList);
		}
		printTeamSchedule(teamsList, teamSchedule);
		return teamSchedule;
	}
	return null;
}

function printTeamSchedule(teams, schedule) {
	teams.forEach(function(teamName) {
		
		var team = schedule[teamName];
		var matches = null;
		for (var j = 0;j < team.matches.length;j++) {
			if (matches == null) {
				matches = team.matches[j];
			} else {
				matches = matches + "," + team.matches[j];
			}
		}
		console.log(team.team + ":" + matches);
	});
}

function isAllStarGame(match) {
	var competitors = match.competitors;
	var team1 = competitors[0].abbreviatedName;
	var team2 = competitors[1].abbreviatedName;
	if ((team1 == "ATL" && team2 == "PAC") || (team2 == "ATL" && team1 == "PAC")) {
		return true;
	}
	return false;
}

function addMatchToSchedule(match, matchList, teamsList) {
	var competitors = match.competitors;
	var team1 = competitors[0].abbreviatedName;
	var team2 = competitors[1].abbreviatedName;
	if (team1 == undefined || team2 == undefined) {
		return;
	}
	if (isAllStarGame(match)) {
		return;
	}

	if (!matchList[team1]) {
		matchList[team1] = {
			team: team1,
			matches: [team2]
		};
	} else {
		matchList[team1].matches.push(team2);
	}
	
	if (!matchList[team2]) {
		matchList[team2] = {
			team: team2,
			matches: [team1]
		};
	} else {
		matchList[team2].matches.push(team1);
	}
	
	if (!teamsList.includes(team1)) {
		teamsList.push(team1);
	}
	if (!teamsList.includes(team2)) {
		teamsList.push(team2);
	}
}

function addStageToSchedule(stage, matchList, teamsList) {
	var matches = stage.matches;
	console.log("Stage has " + matches.length + " matches.");
	var i = 0;
	for (;i < matches.length;i++) {
		var match = matches[i];
		if (match.tournament.type == "OPEN_MATCHES") {
			addMatchToSchedule(match, matchList, teamsList);
		}
	}
}


var configToRead = 'config.json';
var options = {
	loadFirstCalendarOnly: true
};
owl.setOptions(options);
owl.readConfig(configToRead, processCalendar);



